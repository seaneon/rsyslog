FROM alpine:3.17.0

MAINTAINER Alta3 "support@alta3.com"

# copy the rsyslog configuration files into the image
COPY rsyslog.conf /etc/rsyslog.conf
#chmod 644 /etc/rsyslog.conf
#chown root:root /etc/rsyslog.conf
COPY 50-default.conf /etc/rsyslog.d/50-default.conf
#chmod 755 /etc/rsyslog.d
#chown root:root /etc/rsyslog.d



#Start the rsyslog server
RUN systemctl start restart rsyslog.service



